#! /bin/bash
pwd
ls -la
#Вкажіть директорію
echo "write catalog in this directory"
echo "To select current directory write \".\""
#считування назви каталогу
read catalog
#перехід у вказаний каталог
cd $catalog
echo "in catalog"
#Вміст обраного каталогу
ls -l
#Видалення файлів ".tmp" та початковим символом "-" "_" "~"
echo "delete"
rm *.tmp
rm -- -* _* ~*
#Директорія після видалення вмісту
echo "after delete"
ls -l
#Перехід до попереднього каталогу
cd -
